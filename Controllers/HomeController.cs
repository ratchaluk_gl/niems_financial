﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NiemsEBudget.Models;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using NiemsEBudget.WebFunction;
using Newtonsoft.Json.Linq;
using System;
using System.Reflection.Metadata;
using System.Linq.Expressions;

namespace NiemsEBudget.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IConfiguration _configuration;
        private readonly string emsPaymentAPIConf;
        private readonly string emsValuesAPIConf;

        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            _configuration = configuration;
            emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            emsValuesAPIConf = _configuration.GetValue<string>("API:emsValues");
        }

        public IActionResult Index()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login","Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }
            return View();
        }

        public IActionResult DuplicateCheck()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public IActionResult Paid()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public IActionResult Document()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public IActionResult Payment()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public IActionResult PublishDepartment()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public IActionResult AccountManagement()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public IActionResult DepartmentDoc()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return RedirectToAction("Login", "Authen");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            return View();
        }

        public async Task<JsonResult> GetDepartmentPaymentInfo(int? departmentId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            if (String.IsNullOrEmpty(token_ss))
            {
                return Json(new { result = false, message = "token is invalid." });
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            var data = new
            {
                token = token_ss,
                department_ID = departmentId
            };

            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            string? GetDepartmentPaymentAPI = emsPaymentAPIConf + "/GetDepartmentPaymentInfo";

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(GetDepartmentPaymentAPI, jsonData.Replace("\r\n", ""), jwt);

                GetDepartmentPaymentInfoRes? getDepartmentPaymentInfo = new GetDepartmentPaymentInfoRes();
                getDepartmentPaymentInfo = JsonConvert.DeserializeObject<GetDepartmentPaymentInfoRes>(response);

                if (getDepartmentPaymentInfo.callAPIStatus == true)
                {
                    return Json(getDepartmentPaymentInfo);
                }
                else
                {
                    return Json(new { result = false, message = "ไม่พบหน่วยงานที่ค้นหา" });
                }

            }
            catch (Exception ex)
            {
                //log
                return Json(new { result = false, message = ex.ToString() });
            }

        }

        public async Task<JsonResult> SetDepartmentPaymentInfo(string JsonData)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            if (String.IsNullOrEmpty(token_ss))
            {
                return Json(new { result = false, message = "token is invalid." });
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            SetDepartmentPaymentInfoReq setDepartmentPaymentInfo = new SetDepartmentPaymentInfoReq();
            setDepartmentPaymentInfo = JsonConvert.DeserializeObject<SetDepartmentPaymentInfoReq>(JsonData);
            setDepartmentPaymentInfo.token = token_ss;

            string? cJsonData = JsonConvert.SerializeObject(setDepartmentPaymentInfo, Formatting.Indented);
            string setDepartmentAPI = emsPaymentAPIConf + "/SetDepartmentPaymentInfo";

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(setDepartmentAPI, cJsonData.Replace("\r\n", ""), jwt);
                SetDepartmentPaymentInfoRes? setDepartmentPaymentInfoRes = new SetDepartmentPaymentInfoRes();
                setDepartmentPaymentInfoRes = JsonConvert.DeserializeObject<SetDepartmentPaymentInfoRes>(response);

                if (setDepartmentPaymentInfoRes.callAPIStatus == true)
                {
                    return Json(setDepartmentPaymentInfoRes);
                }
                else
                {
                    return Json(setDepartmentPaymentInfoRes);
                }

            }
            catch (Exception ex)
            {
                return Json(new{ callAPIStatus = false, message = ex.ToString() });
            }

        }

        public async Task<JsonResult> PublishDepartmentPaidReport(string? Jsondata)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            if (String.IsNullOrEmpty(token_ss))
            {
                return Json(new {result = false , message = "token is invalid."});
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            PublishDepartmnetPaidReq publishDepartmnetPaid = new PublishDepartmnetPaidReq();
            publishDepartmnetPaid = JsonConvert.DeserializeObject<PublishDepartmnetPaidReq>(Jsondata);
            publishDepartmnetPaid.token = token_ss;
            publishDepartmnetPaid.doc_date_str = Convert.ToDateTime(publishDepartmnetPaid.doc_date_str).ToString("yyyy-MM-dd HH:mm:ss");
            publishDepartmnetPaid.finish_date_str = Convert.ToDateTime(publishDepartmnetPaid.finish_date_str).ToString("yyyy-MM-dd HH:mm:ss");

            string? cJsonData = JsonConvert.SerializeObject(publishDepartmnetPaid , Formatting.Indented);

            string publishDepartmentAPI = emsPaymentAPIConf + "/PublishDepartmentPaidReport";

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(publishDepartmentAPI, cJsonData.Replace("\r\n", ""), jwt);
                PublishDepartmnetPaidRes? publishDepartmnetPaidRes = new PublishDepartmnetPaidRes();
                publishDepartmnetPaidRes = JsonConvert.DeserializeObject<PublishDepartmnetPaidRes>(response);

                if (publishDepartmnetPaidRes.callAPIStatus == true)
                {
                    return Json(new { result = true, message = "ประกาศแจ้งสำเร็จ" });
                }
                else
                {
                    if (publishDepartmnetPaidRes.callAPIStatusMessage.Contains("This schedule has published."))
                    {
                        publishDepartmnetPaidRes.callAPIStatusMessage = "รอบจ่ายนี้ถูกประกาศแล้ว";
                    }
                    return Json(new { result = false, message = publishDepartmnetPaidRes.callAPIStatusMessage });
                }

            }
            catch (Exception ex)
            {
                //log
                return Json(new { result = false, message = ex.ToString() });
            }
        }

        public async Task<JsonResult> SearchPaymentDocInfo(int? provinceId,int? departmentId,string? keyword)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string publishDepartmentAPI = emsPaymentAPIConf + "/SearchPaymentDocInfo";

            var data = new
            {
                token = token_ss,
                s_Province_ID = provinceId,
                s_Department_ID = departmentId,
                s_Keyword = keyword,
                o_By_Condition = "",
                skip = String.IsNullOrEmpty(Request.Form["start"]) ? 0 : (int?)Convert.ToInt32(Request.Form["start"]),
                take = String.IsNullOrEmpty(Request.Form["length"]) ? 0 : (int?)Convert.ToInt32(Request.Form["length"])

            };
            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(publishDepartmentAPI, jsonData, jwt);
                SearchPaymentDocInfoRes searchPaymentDocInfo = new SearchPaymentDocInfoRes();
                searchPaymentDocInfo = JsonConvert.DeserializeObject<SearchPaymentDocInfoRes>(response);

                searchPaymentDocInfo.result.draw = String.IsNullOrEmpty(Request.Form["draw"]) ? 1 : (int?)Convert.ToInt32(Request.Form["draw"]);
                searchPaymentDocInfo.result.recordsTotal = searchPaymentDocInfo.result.count; //for dataTable js
                searchPaymentDocInfo.result.recordsFiltered = searchPaymentDocInfo.result.count; //for dataTable js

                return Json(searchPaymentDocInfo.result);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }

        }

        public async Task<JsonResult> GetPaymentDocInfo(int? docInfoId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string getPaymentDocInfoAPI = emsPaymentAPIConf + "/GetPaymentDocInfo";

            var data = new
            {
                token = token_ss,
                docInfo_ID = docInfoId
            };
            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(getPaymentDocInfoAPI, jsonData, jwt);
                GetPaymentDocInfoRes getPaymentDocInfoRes = new GetPaymentDocInfoRes();
                getPaymentDocInfoRes = JsonConvert.DeserializeObject<GetPaymentDocInfoRes>(response);

                return Json(getPaymentDocInfoRes);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> AddPaymentDocInfo(string? jsonData)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string addDocAPI = emsPaymentAPIConf + "/AddPaymentDocInfo";

            AddPaymentDocInfoReq? paymentDocInfoReq = new AddPaymentDocInfoReq();
            paymentDocInfoReq = JsonConvert.DeserializeObject<AddPaymentDocInfoReq>(jsonData);
            paymentDocInfoReq.token = token_ss;
            try
            {
                string? jsonAddData = JsonConvert.SerializeObject(paymentDocInfoReq, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(addDocAPI, jsonAddData.Replace("\r\n",""), jwt);
                EBDefaultResponse? eBDefault = new EBDefaultResponse();
                eBDefault = JsonConvert.DeserializeObject<EBDefaultResponse>(response);

                return Json(eBDefault);
            }
            catch (Exception ex)
            {
                return Json(new { callAPIStatus = false, message = ex.Message });
            }
        }
        
        public async Task<JsonResult> UpdatePaymentDocInfo(string? jsonData)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string addDocAPI = emsPaymentAPIConf + "/UpdatePaymentDocInfo";

            UpdatePaymentDocInfoReq? paymentDocInfoReq = new UpdatePaymentDocInfoReq();
            paymentDocInfoReq = JsonConvert.DeserializeObject<UpdatePaymentDocInfoReq>(jsonData);
            paymentDocInfoReq.token = token_ss;
            try
            {
                string? jsonAddData = JsonConvert.SerializeObject(paymentDocInfoReq, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(addDocAPI, jsonAddData.Replace("\r\n", ""), jwt);
                EBDefaultResponse? eBDefault = new EBDefaultResponse();
                eBDefault = JsonConvert.DeserializeObject<EBDefaultResponse>(response);

                return Json(eBDefault);
            }
            catch (Exception ex)
            {
                return Json(new { callAPIStatus = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetPaymentDocDepartmentList(int? docInfoId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string getPaymentDocDepartmentListAPI = emsPaymentAPIConf + "/GetPaymentDocDepartmentList";

            var data = new
            {
                token = token_ss,
                docInfo_ID = docInfoId,
                s_Keyword = "",
                o_By_Condition = "",
                skip = String.IsNullOrEmpty(Request.Form["start"]) ? 0 : (int?)Convert.ToInt32(Request.Form["start"]),
                take = String.IsNullOrEmpty(Request.Form["length"]) ? 0 : (int?)Convert.ToInt32(Request.Form["length"])
            };

            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(getPaymentDocDepartmentListAPI, jsonData.Replace("\r\n", ""), jwt);
                DocInfoDepartmentRes? docInfoDepartmentRes = new DocInfoDepartmentRes();
                docInfoDepartmentRes = JsonConvert.DeserializeObject<DocInfoDepartmentRes>(response);

                docInfoDepartmentRes.result.draw = String.IsNullOrEmpty(Request.Form["draw"]) ? 1 : (int?)Convert.ToInt32(Request.Form["draw"]);
                docInfoDepartmentRes.result.recordsTotal = docInfoDepartmentRes.result.count; //for dataTable js
                docInfoDepartmentRes.result.recordsFiltered = docInfoDepartmentRes.result.count; //for dataTable js

                return Json(docInfoDepartmentRes.result);
            }
            catch (Exception ex)
            {
                return Json(new { callAPIStatus = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> DeletePaymentDocInfo(int? docInfoId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string DeletePaymentDocDepartmentListAPI = emsPaymentAPIConf + "/DeletePaymentDocInfo";

            var data = new
            {
                token = token_ss,
                docInfo_ID = docInfoId
            };

            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(DeletePaymentDocDepartmentListAPI, jsonData.Replace("\r\n", ""), jwt);
                EBDefaultResponse eBDefault = new EBDefaultResponse();
                eBDefault = JsonConvert.DeserializeObject<EBDefaultResponse>(response);
                return Json(eBDefault);
            }
            catch (Exception ex)
            {
                return Json(new { callAPIStatus = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetPendingDepartmentsForPaymentDocInfo(int? provinceId,string? keyword)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string getPendingDepartmentListAPI = emsPaymentAPIConf + "/GetPendingDepartmentsForPaymentDocInfo";

            var data = new
            {
                token = token_ss,
                s_Province_ID = provinceId,
                s_Keyword = keyword,
                o_By_Condition = "",
                skip = String.IsNullOrEmpty(Request.Form["start"]) ? 0 : (int?)Convert.ToInt32(Request.Form["start"]),
                take = String.IsNullOrEmpty(Request.Form["length"]) ? 0 : (int?)Convert.ToInt32(Request.Form["length"])
            };

            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(getPendingDepartmentListAPI, jsonData.Replace("\r\n", ""), jwt);
                DocInfoDepartmentRes? docInfoDepartmentRes = new DocInfoDepartmentRes();
                docInfoDepartmentRes = JsonConvert.DeserializeObject<DocInfoDepartmentRes>(response);

                docInfoDepartmentRes.result.draw = String.IsNullOrEmpty(Request.Form["draw"]) ? 1 : (int?)Convert.ToInt32(Request.Form["draw"]);
                docInfoDepartmentRes.result.recordsTotal = docInfoDepartmentRes.result.count; //for dataTable js
                docInfoDepartmentRes.result.recordsFiltered = docInfoDepartmentRes.result.count; //for dataTable js

                return Json(docInfoDepartmentRes.result);
            }
            catch (Exception ex)
            {
                return Json(new { callAPIStatus = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> AddDepartmentsToPaymentDocInfo(string? jsonData)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string addDepartmnetsAPI = emsPaymentAPIConf + "/AddDepartmentToPaymentDocInfo";

            try
            {
                AddDepartmentsToPaymentDocInfoReq? addDepartmentsTo = new AddDepartmentsToPaymentDocInfoReq();
                addDepartmentsTo = JsonConvert.DeserializeObject<AddDepartmentsToPaymentDocInfoReq>(jsonData);
                addDepartmentsTo.token = token_ss;

                string? jsonAddData = JsonConvert.SerializeObject(addDepartmentsTo, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(addDepartmnetsAPI, jsonAddData.Replace("\r\n", ""), jwt);

                EBDefaultResponse eBResponse = new EBDefaultResponse();
                eBResponse = JsonConvert.DeserializeObject<EBDefaultResponse>(response);
                return Json(eBResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, callAPIStatus = false, message = ex.Message });
            }

        }

        public async Task<JsonResult> DeleteDepartmentFromPaymentDocInfo(int? recordId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string DeleteDepartmentFormPaymentDocAPI = emsPaymentAPIConf + "/DeleteDepartmentFromPaymentDocInfo";

            var data = new
            {
                token = token_ss,
                record_ID = recordId
            };

            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(DeleteDepartmentFormPaymentDocAPI, jsonData.Replace("\r\n", ""), jwt);
                EBDefaultResponse eBDefault = new EBDefaultResponse();
                eBDefault = JsonConvert.DeserializeObject<EBDefaultResponse>(response);
                return Json(eBDefault);
            }
            catch (Exception ex)
            {
                return Json(new { callAPIStatus = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetProvince()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string emsValueAPIConf = _configuration.GetValue<string>("API:emsValues");
            string getProvinceAPI = emsValueAPIConf + "/GetAddrProvinceList";

            var data = new
            {
                token = token_ss
            };
            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);

            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(getProvinceAPI, jsonData, jwt);
                AddressObjectResponse addressObjectResponse = new AddressObjectResponse();
                addressObjectResponse = JsonConvert.DeserializeObject<AddressObjectResponse>(response);

                return Json(addressObjectResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetProfile()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Login Fail!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            var data = new
            {
                token = token_ss
            };

            string emsUserAPIConf = _configuration.GetValue<string>("API:user");
            string profileAPI = emsUserAPIConf + "/GetProfile";
            string jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
            try
            {
                GetProfileResult getProfileResult = new GetProfileResult();

                string? getProfileResultStr = await WebFunction.WebFunction.PostDataWithJWT(profileAPI, jsonData, jwt);
                getProfileResult = JsonConvert.DeserializeObject<GetProfileResult>(getProfileResultStr);

                return Json(getProfileResult);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetPaymentData(int? payment_year , int? payment_schedule_id , int? skip , int? take)
        {
            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string searchPayment = emsPaymentAPIConf + "/SearchOperationPayment";
            try
            {
                TokenResponse tokenResponse = new TokenResponse();
                JsonResult? ChecktokenRes = await CheckToken();

                string? JsonDataSerialize = JsonConvert.SerializeObject(ChecktokenRes.Value);

                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(JsonDataSerialize);

                if (tokenResponse.result != true)
                {
                    return Json(new { result = false, message = "Token is Expired" });
                }
                string? token_ss = HttpContext.Session.GetString("_webToken");
                string? jwt = HttpContext.Session.GetString("_jwt");

                EBudgetModel? eBudget = new EBudgetModel();
                //eBudget = JsonConvert.DeserializeObject<EBudgetModel>(jsonData);
                eBudget.token = token_ss;
                eBudget.s_Payment_Year = payment_year;
                eBudget.s_Payment_Status_ID = 4;
                eBudget.s_Payment_Schedule_ID = payment_schedule_id;
                eBudget.s_Province_ID = null;
                eBudget.s_Department_ID = null;
                eBudget.s_Operation_Code = null;
                eBudget.s_Hospital_ID = null;
                eBudget.s_T2_Op_Command_Str_start = null;
                eBudget.s_T2_Op_Command_Str_end = null;
                eBudget.o_By_Condition = "V_DESC";
                eBudget.s_Is_HaveAccount = 1;
                eBudget.s_Level_ID = null;
                eBudget.skip = skip;
                eBudget.take = take;

                string? jsonGetData = JsonConvert.SerializeObject(eBudget, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(searchPayment, jsonGetData.Replace("\r\n", ""), jwt);

                EBudgetResponse? dataTableApprovedOperationDetailResult = new EBudgetResponse();
                dataTableApprovedOperationDetailResult = JsonConvert.DeserializeObject<EBudgetResponse>(response);

                return Json(dataTableApprovedOperationDetailResult);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, callAPIStatus = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetDataTableSearchPayment(string? jsonData)
        {
            
            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string searchPayment = emsPaymentAPIConf + "/SearchOperationPayment";
            try
            {

                TokenResponse tokenResponse = new TokenResponse();
                JsonResult? ChecktokenRes = await CheckToken();

                string? JsonDataSerialize = JsonConvert.SerializeObject(ChecktokenRes.Value);

                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(JsonDataSerialize);

                if (tokenResponse.result != true)
                {
                    return Json(new { result = false, message = "Token is Expired" });
                }
                string? token_ss = HttpContext.Session.GetString("_webToken");
                string? jwt = HttpContext.Session.GetString("_jwt");

                EBudgetModel? eBudget = new EBudgetModel();
                eBudget = JsonConvert.DeserializeObject<EBudgetModel>(jsonData);
                eBudget.token = token_ss;
                eBudget.skip = String.IsNullOrEmpty(Request.Form["start"]) ? eBudget.skip : (int?)Convert.ToInt32(Request.Form["start"]);
                eBudget.take = String.IsNullOrEmpty(Request.Form["length"]) ? eBudget.take : (int?)Convert.ToInt32(Request.Form["length"]);


                string? jsonGetData = JsonConvert.SerializeObject(eBudget, Formatting.Indented);

                string? response = await WebFunction.WebFunction.PostDataWithJWT(searchPayment, jsonGetData.Replace("\r\n", ""), jwt);

                //DataTableApprovedOperationDetail? dataTableApprovedOperationDetail = new DataTableApprovedOperationDetail();
                //List<DataTableApprovedOperationDetail> dataTableApprovedOperationDetailList = new List<DataTableApprovedOperationDetail>();

                EBudgetResponse? dataTableApprovedOperationDetailResult = new EBudgetResponse();
                dataTableApprovedOperationDetailResult = JsonConvert.DeserializeObject<EBudgetResponse>(response);

                dataTableApprovedOperationDetailResult.result.draw = String.IsNullOrEmpty(Request.Form["draw"]) ? 1 : (int?)Convert.ToInt32(Request.Form["draw"]);
                dataTableApprovedOperationDetailResult.result.recordsTotal = dataTableApprovedOperationDetailResult.result.count; //for dataTable js
                dataTableApprovedOperationDetailResult.result.recordsFiltered = dataTableApprovedOperationDetailResult.result.count; //for dataTable js

                return Json(dataTableApprovedOperationDetailResult.result);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, callAPIStatus = false, message = ex.Message });
            }

        }

        public async Task<JsonResult> GetSearchDuplicateOperation(string? jsonData)
        {

            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string searchPayment = emsPaymentAPIConf + "/SearchDuplicateOperation";
            try
            {

                TokenResponse tokenResponse = new TokenResponse();
                JsonResult? ChecktokenRes = await CheckToken();

                string? JsonDataSerialize = JsonConvert.SerializeObject(ChecktokenRes.Value);

                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(JsonDataSerialize);

                if (tokenResponse.result != true)
                {
                    return Json(new { result = false, message = "Token is Expired" });
                }
                string? token_ss = HttpContext.Session.GetString("_webToken");
                string? jwt = HttpContext.Session.GetString("_jwt");

                EBudgetModel? eBudget = new EBudgetModel();
                eBudget = JsonConvert.DeserializeObject<EBudgetModel>(jsonData);
                eBudget.token = token_ss;

                string? jsonGetData = JsonConvert.SerializeObject(eBudget, Formatting.Indented);

                string? response = await WebFunction.WebFunction.PostDataWithJWT(searchPayment, jsonGetData.Replace("\r\n", ""), jwt);

                //DataTableApprovedOperationDetail? dataTableApprovedOperationDetail = new DataTableApprovedOperationDetail();
                //List<DataTableApprovedOperationDetail> dataTableApprovedOperationDetailList = new List<DataTableApprovedOperationDetail>();

                EbudgetSearchDuplicateOperationRes? duplicateData = new EbudgetSearchDuplicateOperationRes();
                duplicateData = JsonConvert.DeserializeObject<EbudgetSearchDuplicateOperationRes>(response);

               

                return Json(duplicateData.result);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, callAPIStatus = false, message = ex.Message });
            }

        }

        public async Task<JsonResult> GetPaymentScheduleList(int? type,int? year,int? is_publish = 1)
        {
            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string paymentSceduleListURL = emsPaymentAPIConf + "/GetPaymentScheduleList";

            TokenResponse tokenResponse = new TokenResponse();
            JsonResult? ChecktokenRes = await CheckToken();

            string? JsonDataSerialize = JsonConvert.SerializeObject(ChecktokenRes.Value);

            tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(JsonDataSerialize);

            if (tokenResponse.result != true)
            {
                return Json(new { result = false, message = "Token is Expired" });
            }
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            var data = new
            {
                token = token_ss,
                s_Payment_Type = type,
                s_Payment_Year = year,
                s_Is_Payment_Published = is_publish
            };
            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
            try
            {
                string? paymentSchListStr = await WebFunction.WebFunction.PostDataWithJWT(paymentSceduleListURL, jsonData, jwt);
                ScheduleListResponse scheduleList = new ScheduleListResponse();
                scheduleList = JsonConvert.DeserializeObject<ScheduleListResponse>(paymentSchListStr);
                return Json(scheduleList);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, callAPIStatus = false, message = ex.Message });
            }

        }

        public async Task<JsonResult> GetHospitalInProvinceList(int? provinceId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string emsValueAPIConf = _configuration.GetValue<string>("API:emsValues");
            string getHospitalAPI = emsValueAPIConf + "/GetHospitalInProvinceList";

            if (provinceId == null)
            {
                provinceId = 0;
            }

            var data = new
            {
                token = token_ss,
                province_Id = provinceId
            };
            string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);


            try
            {
                string? response = await WebFunction.WebFunction.PostDataWithJWT(getHospitalAPI, jsonData, jwt);
                HospitalInProvinceResponse hospitalInProvinceResponse = new HospitalInProvinceResponse();
                hospitalInProvinceResponse = JsonConvert.DeserializeObject<HospitalInProvinceResponse>(response);

                return Json(hospitalInProvinceResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetDepartmentList(int? province_id, int? vehicle_LevelID, int? vehicle_TypeID)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Token Is Expired!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }
            var data = new
            {
                token = token_ss,
                province_id = province_id,
                vehicle_TypeID = vehicle_LevelID,
                vehicle_LevelID = vehicle_TypeID
            };
            string emsValueAPIConf = _configuration.GetValue<string>("API:emsValues");
            string dpmAPI = emsValueAPIConf + "/GetDepartmentList";
            string jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
            try
            {
                DepartmentResult departmentResult = new DepartmentResult();
                List<Department> departmentList = new List<Department>();

                string? departmentResultStr = await WebFunction.WebFunction.PostDataWithJWT(dpmAPI, jsonData, jwt);
                departmentResult = JsonConvert.DeserializeObject<DepartmentResult>(departmentResultStr);

                return Json(departmentResult);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }

        }

        public async Task<JsonResult> GetVehicleLevelList(int? refId)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Token Is Expired!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }
            var data = new
            {
                token = token_ss,
                ref_Id = refId
            };
            string emsValueAPIConf = _configuration.GetValue<string>("API:emsValues");
            string levelAPI = emsValueAPIConf + "/GetVehicleLevelList";
            string jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
            try
            {
                DepartmentResult departmentResult = new DepartmentResult();
                List<Department> departmentList = new List<Department>();

                string? departmentResultStr = await WebFunction.WebFunction.PostDataWithJWT(levelAPI, jsonData, jwt);
                departmentResult = JsonConvert.DeserializeObject<DepartmentResult>(departmentResultStr);

                return Json(departmentResult);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> UpdatePaymentStatus(string? payments)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Token Is Expired!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            UpdatePaymentStatusData updatePaymentStatusData = new UpdatePaymentStatusData();
            updatePaymentStatusData = JsonConvert.DeserializeObject<UpdatePaymentStatusData>(payments);
            updatePaymentStatusData.token = token_ss;

            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string updateAPI = emsPaymentAPIConf + "/UpdateOperationPaymentStatus";
            string jsonData = JsonConvert.SerializeObject(updatePaymentStatusData, Formatting.Indented);
            try
            {
                UpdatePaymentStatusResponse updatePaymentStatusResponse = new UpdatePaymentStatusResponse();
                
                string? updateResponse = await WebFunction.WebFunction.PostDataWithJWT(updateAPI, jsonData.Replace("\r\n", ""), jwt);
                updatePaymentStatusResponse = JsonConvert.DeserializeObject<UpdatePaymentStatusResponse>(updateResponse);

                return Json(updatePaymentStatusResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> UpdateToPaid(string? payments)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Token Is Expired!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            List<int>? paidList = JsonConvert.DeserializeObject<List<int>>(payments);

            UpdatePaymentStatusData updatePaymentStatusData = new UpdatePaymentStatusData();
            //updatePaymentStatusData = JsonConvert.DeserializeObject<UpdatePaymentStatusData>(jsonPaid);
            updatePaymentStatusData.token = token_ss;
            updatePaymentStatusData.operation_IDs = paidList;
            updatePaymentStatusData.to_Status_ID = 5;

            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string updateAPI = emsPaymentAPIConf + "/UpdateOperationPaymentStatus";
            string jsonData = JsonConvert.SerializeObject(updatePaymentStatusData, Formatting.Indented);
            try
            {
                UpdatePaymentStatusResponse updatePaymentStatusResponse = new UpdatePaymentStatusResponse();

                string? updateResponse = await WebFunction.WebFunction.PostDataWithJWT(updateAPI, jsonData.Replace("\r\n", ""), jwt);
                updatePaymentStatusResponse = JsonConvert.DeserializeObject<UpdatePaymentStatusResponse>(updateResponse);

                return Json(updatePaymentStatusResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }

        public async Task<JsonResult> GetAccountTypeList()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Token Is Expired!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            string accountlistStr = emsValuesAPIConf + "/GetAccountTypeList";
            var data = new
            {
                token = token_ss,
            };

            try
            {
                EbudgetValuesRes? EbResponse = new EbudgetValuesRes();
                string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(accountlistStr, jsonData, jwt);

                EbResponse = JsonConvert.DeserializeObject<EbudgetValuesRes>(response);

                return Json(EbResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.ToString() });
            }

        }

        public async Task<JsonResult> GetAccountBankList()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            if (String.IsNullOrEmpty(token_ss))
            {
                return Json("Token Is Expired!");
            }
            else
            {
                HttpContext.Session.SetString("_webToken", token_ss);
                HttpContext.Session.SetString("_jwt", jwt);
            }

            string accountBankListStr = emsValuesAPIConf + "/GetAccountBankList";

            var data = new
            {
                token = token_ss,
            };

            try
            {
                EbudgetValuesRes? EbResponse = new EbudgetValuesRes();
                string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(accountBankListStr, jsonData, jwt);

                EbResponse = JsonConvert.DeserializeObject<EbudgetValuesRes>(response);

                return Json(EbResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.ToString() });
            }

        }

        public async Task<JsonResult> CheckToken()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            if (String.IsNullOrEmpty(token_ss))
            {
                return Json(new { result = false, message = "FLI" });
            }

            string userAPIConf = _configuration.GetValue<string>("API:user");
            string checkToken = userAPIConf + "/CheckToken";

            var data = new
            {
                token = token_ss,
            };

            try
            {
                TokenResponse? tokenResponse = new TokenResponse();
                string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(checkToken, jsonData, jwt);

                tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(response);
                tokenResponse.result = true;
                tokenResponse.tok = token_ss;
                tokenResponse.jwt = jwt;

                return Json(tokenResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = "Token หมดอายุ กรุณาเข้าสู่ระบบอีกครั้ง" });
            }

        }

        public async Task<JsonResult> GetReportFile(int? schedule,int? reportType)
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            if (String.IsNullOrEmpty(token_ss))
            {
                return Json(new { result = false, message = "Token หมดอายุ กรุณาเข้าสู่ระบบอีกครั้ง" });
                //return null;
            }

            var data = new
            {
                token = token_ss,
                schedule_ID = schedule,
                reportType_ID = reportType,
                result_as_File = 0
            };

            string? reportTypeName = "/GetPaidReport";

            switch (reportType)
            {
                case 8:
                    reportTypeName = "/GetPaidSummaryReport_NotDirectPayProvince";
                    break;
                case 9:
                    reportTypeName = "/GetPaidSummaryReport_DirectPayProvince";
                    break;
                case 10:
                    reportTypeName = "/GetPaidSummaryReport_ByPayType";
                    break;
                case 11:
                    reportTypeName = "/GetPaidSummaryReport_ByLevel";
                    break;
                default:
                    reportTypeName = "/GetPaidReport";
                    break;
            }

            string emsPaymentAPIConf = _configuration.GetValue<string>("API:emsPayment");
            string GetPaidReport = emsPaymentAPIConf + reportTypeName;

            try
            {
                TokenResponse? tokenResponse = new TokenResponse();
                string? jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
                string? response = await WebFunction.WebFunction.PostDataWithJWT(GetPaidReport, jsonData, jwt);

                PaidReportResponse paidReportResponse = new PaidReportResponse();

                string? reportResponseStr = await WebFunction.WebFunction.PostDataWithJWT(GetPaidReport, jsonData.Replace("\r\n", ""), jwt);
                paidReportResponse = JsonConvert.DeserializeObject<PaidReportResponse>(reportResponseStr);

                var bytes = Convert.FromBase64String(paidReportResponse.result.fileContent);
                paidReportResponse.result.fileContentBytes = bytes;
                //return File(bytes, paidReportResponse.result.fileType,paidReportResponse.result.fileName);
                return Json(paidReportResponse);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = "เกิดข้อผิดพลาด :"+ ex.ToString() });
                //return null;
            }
        }

    }
}