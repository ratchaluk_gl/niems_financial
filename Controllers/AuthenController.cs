﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NiemsEBudget.Models;
using NiemsEBudget.WebFunction;

namespace NiemsEBudget.Controllers
{
    public class AuthenController : Controller
    {
        private IConfiguration _configuration;
        public string SST = "_webToken";
        public string JWT = "_jwt";

        public AuthenController(IConfiguration iConfig)
        {
            _configuration = iConfig;
        }
        public async Task<IActionResult> Login(LoginData loginData)
        {
            if (ModelState.IsValid)
            {
                string appCode = _configuration.GetValue<string>("API:application_code");
                loginData.application_code = appCode;

                string userAPIConf = _configuration.GetValue<string>("API:user");
                string loginAPI = userAPIConf + "/Login";
                string jsonData = JsonConvert.SerializeObject(loginData);
                try
                {
                    string loginResultStr = await WebFunction.WebFunction.PostData(loginAPI, jsonData);
                    LoginResult? loginResult = JsonConvert.DeserializeObject<LoginResult>(loginResultStr);

                    if (loginResult.callAPIStatus)
                    {
                        HttpContext.Session.SetString(SST, loginResult.Result.webToken);
                        HttpContext.Session.SetString(JWT, loginResult.Result.jwtToken);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Message = loginResult.callAPIStatusMessage;
                        return View("Login", loginData);
                    }
                    
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex;
                    return View("Login", loginData);
                }
            }
            else
            {
                return View("Login", loginData);
            }
            
        }
        public async Task<IActionResult> Logout()
        {
            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            string userAPIConf = _configuration.GetValue<string>("API:user");
            string logoutAPI = userAPIConf + "/Logout";

            var data = new
            {
                token = token_ss
            };

            string jsonData = JsonConvert.SerializeObject(data);
            string loginResultStr = await WebFunction.WebFunction.PostData(logoutAPI, jsonData);

            TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(loginResultStr);

            return RedirectToAction("Login", "Authen");
        }

    }
}
