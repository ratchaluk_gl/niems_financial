﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NiemsEBudget.Models;

namespace NiemsEBudget.Controllers
{
    public class PreviewController : Controller
    {
        private readonly ILogger<PreviewController> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IConfiguration _configuration;

        public PreviewController(ILogger<PreviewController> logger, IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            _configuration = configuration;
        }
        public async Task<IActionResult> Operation(int? operationId)
        {
            //Yellow
            string emsUpdateAPIConf = _configuration.GetValue<string>("API:emsUpdate");
            string getOperation = emsUpdateAPIConf + "/GetOperationById" + '/' + operationId;

            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            try
            {
                string? response = await WebFunction.WebFunction.GetDataWithJWT(getOperation , jwt);
                YellowOperationResultPreviewPage? yellowOperationResultPreviewPage = new YellowOperationResultPreviewPage();
                yellowOperationResultPreviewPage = JsonConvert.DeserializeObject<YellowOperationResultPreviewPage>(response);

                return View(yellowOperationResultPreviewPage);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }

            //return View();
            //return RedirectToAction("Green", "Preview", new {operationId = operationId});
        }
        public IActionResult MainVictim(int? vic)
        {
            if (vic == 444)
            {
                return RedirectToAction("Blue", "Preview", new { victimId = vic });
            }
            else
            {
                return RedirectToAction("Green", "Preview", new { victimId = vic });
            }
            
        }
        public async Task<IActionResult> Green(int? op ,int? victimId)
        {
            //ViewBag.Victim = victimId;
            string emsUpdateAPIConf = _configuration.GetValue<string>("API:emsUpdate");
            string getGreen = emsUpdateAPIConf + "/GetOperationGreenById" + '/' + op + '/'+ victimId;

            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");
            try
            {
                string? response = await WebFunction.WebFunction.GetDataWithJWT(getGreen, jwt);
                GreenOperationResultPreviewPage? greenOperationResultPreviewPage = new GreenOperationResultPreviewPage();
                greenOperationResultPreviewPage = JsonConvert.DeserializeObject<GreenOperationResultPreviewPage>(response);

                return View(greenOperationResultPreviewPage);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }

        }
        public async Task<IActionResult> Blue(string? op ,int? victimId)
        {
            string emsUpdateAPIConf = _configuration.GetValue<string>("API:emsUpdate");
            string getBlue = emsUpdateAPIConf + "/GetOperationBlueById" + '/' + op + '/' + victimId;

            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            try
            {
                string? response = await WebFunction.WebFunction.GetDataWithJWT(getBlue, jwt);
                BlueOperationResultPreviewPage? blueOperationResultPreviewPage = new BlueOperationResultPreviewPage();
                blueOperationResultPreviewPage = JsonConvert.DeserializeObject<BlueOperationResultPreviewPage>(response);

                return View(blueOperationResultPreviewPage);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }
        public async Task<IActionResult> Pink(string? op, int? victimId)
        {
            string emsUpdateAPIConf = _configuration.GetValue<string>("API:emsUpdate");
            string getPink = emsUpdateAPIConf + "/GetOperationPinkById" + '/' + op + '/' + victimId;

            string? token_ss = HttpContext.Session.GetString("_webToken");
            string? jwt = HttpContext.Session.GetString("_jwt");

            try
            {
                string? response = await WebFunction.WebFunction.GetDataWithJWT(getPink, jwt);
                PinkOperationResultPreviewPage? pinkOperationResultPreviewPage = new PinkOperationResultPreviewPage();
                pinkOperationResultPreviewPage = JsonConvert.DeserializeObject<PinkOperationResultPreviewPage>(response);

                return View(pinkOperationResultPreviewPage);
            }
            catch (Exception ex)
            {
                return Json(new { result = false, message = ex.Message });
            }
        }
       
    }
}
