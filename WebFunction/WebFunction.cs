﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NiemsEBudget.Models;

namespace NiemsEBudget.WebFunction
{
    public class WebFunction
    {
        public static async Task<string> PostData(string apiURL,string DataJson)
        {
            string? retData = "";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    StringContent? contentData = null;
                    if (!string.IsNullOrEmpty(DataJson))
                    {
                        contentData = new StringContent(DataJson, Encoding.UTF8, "application/json");
                    }
                    HttpResponseMessage response = await client.PostAsync(apiURL, contentData);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        retData = data;
                    }
                    else
                    {
                        retData = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return retData;
        }
        public static async Task<string> PostDataWithJWT(string apiURL, string DataJson, string Bearer)
        {
            string? retData = "";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Bearer);

                    StringContent? contentData = null;
                    if (!string.IsNullOrEmpty(DataJson))
                    {
                        contentData = new StringContent(DataJson, Encoding.UTF8, "application/json");
                        //contentData.Headers.Add("Authorization", "Bearer " + Bearer);\
                    }
                    HttpResponseMessage response = await client.PostAsync(apiURL, contentData);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        retData = data;
                    }
                    else
                    {
                        retData = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return retData;
        }

        public static async Task<string> GetDataWithJWT(string apiURL, string Bearer)
        {
            string? retData = "";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Bearer);

                    //StringContent? contentData = null;
                    //if (!string.IsNullOrEmpty(DataJson))
                    //{
                    //    contentData = new StringContent(DataJson, Encoding.UTF8, "application/json");
                    //    //contentData.Headers.Add("Authorization", "Bearer " + Bearer);\
                    //}
                    HttpResponseMessage response = await client.GetAsync(apiURL);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        retData = data;
                    }
                    else
                    {
                        retData = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return retData;
        }
        

    }
}
