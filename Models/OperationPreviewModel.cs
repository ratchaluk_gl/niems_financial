﻿namespace NiemsEBudget.Models
{
    public class OperationPreviewModel
    {

    }
    public class YellowOperationResultPreviewPage
    {
        public YellowOperationPreviewSection1? sec1 { get; set; }
        public YellowOperationPreviewSection2? sec2 { get; set; }
        public YellowOperationPreviewSection3? sec3 { get; set; }
        public YellowOperationPreviewSection4? sec4 { get; set; }
        public YellowOperationPreviewSection5? sec5 { get; set; }
        public YellowOperationPreviewSection6? sec6 { get; set; }
        public YellowOperationPreviewSection7? sec7 { get; set; }
        public YellowOperationPreviewSection8? sec8 { get; set; }
        public YellowOperationPreviewSection9? sec9 { get; set; }
        public string? updated { get; set; }
        public string? updateBy { get; set; }
    }

    public class YellowOperationPreviewSection1
    {
        public int? eventId { get; set; }
        public int? eventOperationId { get; set; }
        public string? eventTime { get; set; }
        public string? eventCode { get; set; }
        public string? operationCode { get; set; }
        public int? puiNumber { get; set; }
        public int? eventChannel { get; set; }
        public string? callerName { get; set; }
        public string? callerNumber { get; set; }
        public int? eventProvinceId { get; set; }
        public int? provinceId { get; set; }
        public string? provinceName { get; set; }
        public int? districtId { get; set; }
        public string? districtName { get; set; }
        public int? subDistrictId { get; set; }
        public string? subDistrictName { get; set; }
        public string? addrDetail { get; set; }
    }
    public class YellowOperationPreviewSection2
    {
        public int? stateId { get; set; }
    }
    public class YellowOperationPreviewSection3
    {
        public string? stateDetail { get; set; }
    }
    public class YellowOperationPreviewSection4
    {
        public int? stateId { get; set; }
        public int? stateIdcId { get; set; }
        public string? stateIdcName { get; set; }
        public int? erId { get; set; }
    }
    public class YellowOperationPreviewSection5
    {
        public int? vehicleTypeId { get; set; }
        public int? departmentId { get; set; }
        public string? departmentName { get; set; }
        public int? vehicleId { get; set; }
        public string? vehicleName { get; set; }
        public int? vehicleLevelId { get; set; }
        public string? vehicleLevelName { get; set; }
        public string? startPoint { get; set; }
        public string? stopPoint { get; set; }
        public string? time1Call { get; set; }
        public string? lat1Call { get; set; }
        public string? lon1Call { get; set; }
        public string? time2Command { get; set; }
        public string? lat2Command { get; set; }
        public string? lon2Command { get; set; }
        public string? time3OutBase { get; set; }
        public string? lat3OutBase { get; set; }
        public string? lon3OutBase { get; set; }
        public string? time4AtPoint { get; set; }
        public string? lat4AtPoint { get; set; }
        public string? lon4AtPoint { get; set; }
        public string? time5OutPoint { get; set; }
        public string? lat5OutPoint { get; set; }
        public string? lon5OutPoint { get; set; }
        public string? time6AtHos { get; set; }
        public string? lat6AtHos { get; set; }
        public string? lon6AtHos { get; set; }
        public string? time7AtBase { get; set; }
        public string? lat7AtBase { get; set; }
        public string? lon7AtBase { get; set; }
        public int? km1AtBase { get; set; }
        public int? km2AtPoint { get; set; }
        public int? km3AtHos { get; set; }
        public int? km4AtBase { get; set; }
        public int? staff4DocId { get; set; }
        public string? staff4DocName { get; set; }
        public int? staff5NurseId { get; set; }
        public string? staff5NurseName { get; set; }
        public int? staff1Id { get; set; }
        public string? staff1Name { get; set; }
        public int? staff2Id { get; set; }
        public string? staff2Name { get; set; }
        public int? staff3Id { get; set; }
        public string? staff3Name { get; set; }
    }
    public class YellowOperationPreviewSection6
    {
        public int? stateRcMainId { get; set; }
        public string? stateRcMainName { get; set; }
        public int? stateRcId { get; set; }
        public string? stateRcName { get; set; }
    }
    public class YellowOperationPreviewSection7
    {
        public int? serviceId { get; set; }
    }
    public class YellowOperationPreviewSection8
    {
        public List<VictimDetail>? victims { get; set; }
    }
    public class YellowOperationPreviewSection9
    {
        public string? icarLabelGroup { get; set; }
        public int? icarLabelNumber { get; set; }
        public int? icarLabelProvinceId { get; set; }
        public string? icarLabelProvinceName { get; set; }
        public int? icarIsOwner { get; set; }
        public int? createdBy { get; set; }
        public string? createdByName { get; set; }
        public string? created { get; set; }
        public int? approveByDepId { get; set; }
        public string? approveByDepName { get; set; }
        public int? approveBy { get; set; }
        public string? approveByName { get; set; }
    }
    public class VictimDetail
    {
        public int? id { get; set; }
        public int? victimeTitleId { get; set; }
        public string? victimFirstname { get; set; }
        public string? victimLastname { get; set; }
        public int? victimAge { get; set; }
        public string? hnCode { get; set; }
        public string? peopleIdCard { get; set; }
        public int? transferHospitalProvinceId { get; set; }
        public string? transferHospitalProvinceName { get; set; }
        public int? transferHospitalId { get; set; }
        public string? transferHospitalName { get; set; }
        public int? transferReason1 { get; set; }
        public int? transferReason2 { get; set; }
        public int? transferReason3 { get; set; }
        public int? transferReason4 { get; set; }
        public int? transferReason5 { get; set; }
        public int? isMain { get; set; }
        public int? hospitalContChannelId { get; set; }
        public int? stateCount { get; set; }
    }

    public class BlueOperationResultPreviewPage
    {
        public BlueOperationPreviewSection1? sec1 { get; set; }
        public BlueOperationPreviewSection2? sec2 { get; set; }
        public BlueOperationPreviewSection3? sec3 { get; set; }
        public BlueOperationPreviewSection4? sec4 { get; set; }
        public BlueOperationPreviewSection5? sec5 { get; set; }
        public BlueOperationPreviewSection6? sec6 { get; set; }
        public string? updated { get; set; }
        public string? updateBy { get; set; }
    }
    public class BlueOperationPreviewSection1
    {
        public int? eventId { get; set; }
        public int? eventOperationId { get; set; }
        public string? victimCode { get; set; }
        public string? eventTime { get; set; }
        public string? eventCode { get; set; }
        public string? operationCode { get; set; }
        public int? eventProvinceId { get; set; }
        public int? provinceId { get; set; }
        public int? departmentId { get; set; }
        public string? departmentName { get; set; }
        public int? vehicleId { get; set; }
        public int? staff4DocId { get; set; }
        public string? staff4DocName { get; set; }
        public int? staff5NurseId { get; set; }
        public string? staff5NurseName { get; set; }
        public int? staff1Id { get; set; }
        public string? staff1Name { get; set; }
        public int? staff2Id { get; set; }
        public string? staff2Name { get; set; }
        public int? staff3Id { get; set; }
        public string? staff3Name { get; set; }
        public int? stateIsFound { get; set; }
        public string? addrDetail { get; set; }
        public string? provinceName { get; set; }
        public int? districtId { get; set; }
        public string? districtName { get; set; }
        public int? subDistrictId { get; set; }
        public string? subDistrictName { get; set; }
        public string? eventRemark { get; set; }
    }
    public class BlueOperationPreviewSection2
    {
        public int? vehicleTypeId { get; set; }
        public int? departmentId { get; set; }
        public int? vehicleId { get; set; }
        public string? vehicleName { get; set; }
        public int? vehicleLevelId { get; set; }
        public string? vehicleLevelName { get; set; }
        public string? startPoint { get; set; }
        public string? stopPoint { get; set; }
        public string? time1Call { get; set; }
        public string? lat1Call { get; set; }
        public string? lon1Call { get; set; }
        public string? time2Command { get; set; }
        public string? lat2Command { get; set; }
        public string? lon2Command { get; set; }
        public string? time3OutBase { get; set; }
        public string? lat3OutBase { get; set; }
        public string? lon3OutBase { get; set; }
        public string? time4AtPoint { get; set; }
        public string? lat4AtPoint { get; set; }
        public string? lon4AtPoint { get; set; }
        public string? time5OutPoint { get; set; }
        public string? lat5OutPoint { get; set; }
        public string? lon5OutPoint { get; set; }
        public string? time6AtHos { get; set; }
        public string? lat6AtHos { get; set; }
        public string? lon6AtHos { get; set; }
        public string? time7AtBase { get; set; }
        public string? lat7AtBase { get; set; }
        public string? lon7AtBase { get; set; }
        public int? km1AtBase { get; set; }
        public int? km2AtPoint { get; set; }
        public int? km3AtHos { get; set; }
        public int? km4AtBase { get; set; }
    }
    public class BlueOperationPreviewSection3
    {
        public int? victimId { get; set; }
        public int? victimeTitleId { get; set; }
        public string? victimeTitleName { get; set; }
        public string? victimFirstname { get; set; }
        public string? victimLastname { get; set; }
        public int? victimAge { get; set; }
        public int? genderId { get; set; }
        public object? peopleTypeId { get; set; }
        public string? peopleIdCard { get; set; }
        public object? peopleCountryId { get; set; }
        public object? peopleCountryName { get; set; }
        public object? passport { get; set; }
        public object? treatmentRightId { get; set; }
        public int? isItravelId { get; set; }
        public object? itravelCountryId { get; set; }
        public object? itravelCountryName { get; set; }
        public object? icarCountryId { get; set; }
        public object? icarLabelGroup { get; set; }
        public object? icarLabelNumber { get; set; }
        public object? icarLabelProvinceId { get; set; }
        public object? icarLabelProvinceName { get; set; }
        public int? icarIsOwner { get; set; }
        public object? icarTypeId { get; set; }
        public object? icarTypeOwnerName { get; set; }
        public object? victimTypeId { get; set; }
        public object? sensationOk { get; set; }
        public object? sensationDull { get; set; }
        public object? sensationFaintOk { get; set; }
        public object? sensationFaintNo { get; set; }
        public object? sensationNoisy { get; set; }
        public object? breathNormal { get; set; }
        public object? breathFast { get; set; }
        public object? breathSlow { get; set; }
        public object? breathRough { get; set; }
        public object? breathNo { get; set; }
        public object? woundNo { get; set; }
        public object? woundAbrasion { get; set; }
        public object? woundCut { get; set; }
        public object? woundContusion { get; set; }
        public object? woundBurn { get; set; }
        public object? woundStab { get; set; }
        public object? woundGsw { get; set; }
        public object? woundAmputate { get; set; }
        public object? woundExplode { get; set; }
        public object? boneNo { get; set; }
        public object? boneYes { get; set; }
        public object? organHead { get; set; }
        public object? organFace { get; set; }
        public object? organSpine { get; set; }
        public object? organChest { get; set; }
        public object? organAbdomen { get; set; }
        public object? organPelvis { get; set; }
        public object? organExtremities { get; set; }
        public object? organExtSurface { get; set; }
        public object? organMultiinjuryBack { get; set; }
        public object? svBreathNo { get; set; }
        public object? svBreathClear { get; set; }
        public object? svBreathOral { get; set; }
        public object? svBreathO2 { get; set; }
        public object? svBreathAmbubag { get; set; }
        public object? svBreathPockedMask { get; set; }
        public object? svWoundNo { get; set; }
        public object? svWoundPressure { get; set; }
        public object? svWoundDressing { get; set; }
        public object? svBoneNo { get; set; }
        public object? svBoneSling { get; set; }
        public object? svBoneSpinal { get; set; }
        public object? svBoneKed { get; set; }
        public object? svCprNo { get; set; }
        public object? svCprYes { get; set; }
        public object? primaryResultId { get; set; }
    }
    public class BlueOperationPreviewSection4
    {
        public int? transferHospitalProvinceId { get; set; }
        public int? transferHospitalId { get; set; }
        public string? transferHospitalName { get; set; }
        public int? transferHospitalTypeId { get; set; }
        public int? transferReason1 { get; set; }
        public int? transferReason2 { get; set; }
        public int? transferReason3 { get; set; }
        public int? transferReason4 { get; set; }
        public int? transferReason5 { get; set; }
        public object? reportBy { get; set; }
        public object? reportByName { get; set; }
    }
    public class BlueOperationPreviewSection5
    {
        public string? hnCode { get; set; }
        public string? diagnosis { get; set; }
        public int? erId { get; set; }
        public int? assessRespiratoryId { get; set; }
        public string? assessCommentRespiratory { get; set; }
        public int? assessHemorrhageId { get; set; }
        public string? assessCommentHemorrhage { get; set; }
        public int? assessFluidId { get; set; }
        public int? assessCommentFluid { get; set; }
        public int? assessBoneId { get; set; }
        public string? assessCommentBone { get; set; }
        public int? assessorPosition { get; set; }
        public int? assessorId { get; set; }
        public object? assessorName { get; set; }
    }
    public class BlueOperationPreviewSection6
    {
        public int? isAdmit { get; set; }
        public int? finalResultId { get; set; }
        public int? stateId { get; set; }
        public int? stateIdcId { get; set; }
        public int? stateRcMainId { get; set; }
        public int? stateRcId { get; set; }
    }

    public class GreenOperationResultPreviewPage
    {
        public GreenOperationPreviewSection1? sec1 { get; set; }
        public GreenOperationPreviewSection2? sec2 { get; set; }
        public GreenOperationPreviewSection3? sec3 { get; set; }
        public GreenOperationPreviewSection4? sec4 { get; set; }
        public GreenOperationPreviewSection5? sec5 { get; set; }
        public GreenOperationPreviewSection6? sec6 { get; set; }
        public string? updated { get; set; }
        public string? updateBy { get; set; }
    }

    public class GreenOperationPreviewSection1
    {
        public int? eventId { get; set; }
        public int? eventOperationId { get; set; }
        public string? victimCode { get; set; }
        public string? eventTime { get; set; }
        public string? eventCode { get; set; }
        public string? operationCode { get; set; }
        public int? eventProvinceId { get; set; }
        public int? provinceId { get; set; }
        public int? departmentId { get; set; }
        public string? departmentName { get; set; }
        public int? vehicleId { get; set; }
        public int? staff4DocId { get; set; }
        public string? staff4DocName { get; set; }
        public int? staff5NurseId { get; set; }
        public string? staff5NurseName { get; set; }
        public int? staff1Id { get; set; }
        public string? staff1Name { get; set; }
        public int? staff2Id { get; set; }
        public string? staff2Name { get; set; }
        public int? staff3Id { get; set; }
        public string? staff3Name { get; set; }
        public int? stateIsFound { get; set; }
        public string? addrDetail { get; set; }
        public string? provinceName { get; set; }
        public int? districtId { get; set; }
        public string? districtName { get; set; }
        public int? subDistrictId { get; set; }
        public string? subDistrictName { get; set; }
        public string? eventRemark { get; set; }
    }
    public class GreenOperationPreviewSection2
    {
        public int? vehicleTypeId { get; set; }
        public int? departmentId { get; set; }
        public int? vehicleId { get; set; }
        public string? vehicleName { get; set; }
        public int? vehicleLevelId { get; set; }
        public string? vehicleLevelName { get; set; }
        public string? startPoint { get; set; }
        public string? stopPoint { get; set; }
        public string? time1Call { get; set; }
        public string? lat1Call { get; set; }
        public string? lon1Call { get; set; }
        public string? time2Command { get; set; }
        public string? lat2Command { get; set; }
        public string? lon2Command { get; set; }
        public string? time3OutBase { get; set; }
        public string? lat3OutBase { get; set; }
        public string? lon3OutBase { get; set; }
        public string? time4AtPoint { get; set; }
        public string? lat4AtPoint { get; set; }
        public string? lon4AtPoint { get; set; }
        public string? time5OutPoint { get; set; }
        public string? lat5OutPoint { get; set; }
        public string? lon5OutPoint { get; set; }
        public string? time6AtHos { get; set; }
        public string? lat6AtHos { get; set; }
        public string? lon6AtHos { get; set; }
        public string? time7AtBase { get; set; }
        public string? lat7AtBase { get; set; }
        public string? lon7AtBase { get; set; }
        public int? km1AtBase { get; set; }
        public int? km2AtPoint { get; set; }
        public int? km3AtHos { get; set; }
        public int? km4AtBase { get; set; }
    }
    public class GreenOperationPreviewSection3
    {
        public int? victimId { get; set; }
        public int? victimeTitleId { get; set; }
        public string? victimeTitleName { get; set; }
        public string? victimFirstname { get; set; }
        public string? victimLastname { get; set; }
        public int? victimAge { get; set; }
        public int? genderId { get; set; }
        public int? peopleTypeId { get; set; }
        public string? peopleIdCard { get; set; }
        public int? peopleCountryId { get; set; }
        public string? peopleCountryName { get; set; }
        public string? passport { get; set; }
        public int? treatmentRightId { get; set; }
        public int? isItravelId { get; set; }
        public int? itravelCountryId { get; set; }
        public string? itravelCountryName { get; set; }
        public int? icarCountryId { get; set; }
        public string? icarLabelGroup { get; set; }
        public int? icarLabelNumber { get; set; }
        public int? icarLabelProvinceId { get; set; }
        public string? icarLabelProvinceName { get; set; }
        public int? icarIsOwner { get; set; }
        public int? icarTypeId { get; set; }
        public string? icarTypeOwnerName { get; set; }
        public string? a1Time { get; set; }
        public string? a1VsignT { get; set; }
        public string? a1VsignBp { get; set; }
        public string? a1VsignPr { get; set; }
        public string? a1VsignRr { get; set; }
        public string? a1NsignE { get; set; }
        public string? a1NsignV { get; set; }
        public string? a1NsignM { get; set; }
        public string? a1PupilRt { get; set; }
        public string? a1PupilLtl { get; set; }
        public string? a1PupilLt { get; set; }
        public string? a1PupilRtl { get; set; }
        public string? a1O2Sat { get; set; }
        public string? a1Dtx { get; set; }
        public string? a2Time { get; set; }
        public string? a2VsignT { get; set; }
        public string? a2VsignBp { get; set; }
        public string? a2VsignPr { get; set; }
        public string? a2VsignRr { get; set; }
        public string? a2NsignE { get; set; }
        public string? a2NsignV { get; set; }
        public string? a2NsignM { get; set; }
        public string? a2PupilRt { get; set; }
        public string? a2PupilLtl { get; set; }
        public string? a2PupilLt { get; set; }
        public string? a2PupilRtl { get; set; }
        public string? a2O2Sat { get; set; }
        public string? a2Dtx { get; set; }
        public string? a3Time { get; set; }
        public string? a3VsignT { get; set; }
        public string? a3VsignBp { get; set; }
        public string? a3VsignPr { get; set; }
        public string? a3VsignRr { get; set; }
        public string? a3NsignE { get; set; }
        public string? a3NsignV { get; set; }
        public string? a3NsignM { get; set; }
        public string? a3PupilRt { get; set; }
        public string? a3PupilLtl { get; set; }
        public string? a3PupilLt { get; set; }
        public string? a3PupilRtl { get; set; }
        public string? a3O2Sat { get; set; }
        public string? a3Dtx { get; set; }
        public int? woundNo { get; set; }
        public int? woundCut { get; set; }
        public int? woundAbrasion { get; set; }
        public int? woundContusion { get; set; }
        public int? woundBurn { get; set; }
        public int? woundStab { get; set; }
        public int? woundAmputate { get; set; }
        public int? woundGsw { get; set; }
        public int? boneNo { get; set; }
        public int? boneClosed { get; set; }
        public int? boneOpened { get; set; }
        public int? boneDislocate { get; set; }
        public int? bloodNo { get; set; }
        public int? bloodExtStop { get; set; }
        public int? bloodExtActive { get; set; }
        public int? bloodHemorrhage { get; set; }
        public int? organHead { get; set; }
        public int? organFace { get; set; }
        public int? organSpine { get; set; }
        public int? organChest { get; set; }
        public int? organAbdomen { get; set; }
        public int? organPelvis { get; set; }
        public int? organExtremities { get; set; }
        public int? organExtSurface { get; set; }
        public int? organMultiinjuryBack { get; set; }
        public int? medDyspnea { get; set; }
        public int? medFever { get; set; }
        public int? medAlterConscious { get; set; }
        public int? medSeizure { get; set; }
        public int? medChestpain { get; set; }
        public int? medPoison { get; set; }
        public int? medDigestive { get; set; }
        public int? medOther { get; set; }
        public string? medOtherDetail { get; set; }
        public int? obsBirth { get; set; }
        public int? obsBleed { get; set; }
        public int? obsPreg { get; set; }
        public int? obsRape { get; set; }
        public int? obsOther { get; set; }
        public string? obsOtherDetail { get; set; }
        public int? pedConvulsion { get; set; }
        public int? pedFever { get; set; }
        public int? pedDyspnea { get; set; }
        public int? pedDigestive { get; set; }
        public int? pedOther { get; set; }
        public string? pedOtherDetail { get; set; }
        public int? surAbdominal { get; set; }
        public int? surBleed { get; set; }
        public int? surOther { get; set; }
        public string? surOtherDetail { get; set; }
        public int? otherEye { get; set; }
        public int? otherEnt { get; set; }
        public int? otherOrtho { get; set; }
        public int? otherPsycho { get; set; }
        public int? svBreathNo { get; set; }
        public int? svBreathClear { get; set; }
        public int? svBreathSuction { get; set; }
        public int? svBreathOral { get; set; }
        public int? svBreathO2 { get; set; }
        public int? svBreathAmbubag { get; set; }
        public int? svBreathEt { get; set; }
        public int? svWoundNo { get; set; }
        public int? svWoundPressure { get; set; }
        public int? svWoundDressing { get; set; }
        public int? svFluidNo { get; set; }
        public int? svFluidNss { get; set; }
        public int? svFluidRls { get; set; }
        public int? svFluid5dn2 { get; set; }
        public int? svFluidOnlock { get; set; }
        public int? svFluidOther { get; set; }
        public string? svFluidOtherDetail { get; set; }
        public int? svBoneNo { get; set; }
        public int? svBoneSling { get; set; }
        public int? svBoneSpinal { get; set; }
        public int? svBoneKed { get; set; }
        public int? svCprNo { get; set; }
        public int? svCprYes { get; set; }
        public string? wounSvCprAeddGsw { get; set; }
        public string? drugComment { get; set; }
        public int? primaryResultId { get; set; }
        public int? stateRcMainId { get; set; }
        public string? stateRcMainName { get; set; }
        public int? stateRcId { get; set; }
        public string? stateRcName { get; set; }
    }

    public class GreenOperationPreviewSection4
    {
        public int? transferHospitalProvinceId { get; set; }
        public int? transferHospitalId { get; set; }
        public string? transferHospitalName { get; set; }
        public int? transferHospitalTypeId { get; set; }
        public int? transferReason1 { get; set; }
        public int? transferReason2 { get; set; }
        public int? transferReason3 { get; set; }
        public int? transferReason4 { get; set; }
        public int? transferReason5 { get; set; }
        public int? reportBy { get; set; }
        public string? reportByName { get; set; }
    }
    public class GreenOperationPreviewSection5
    {
        public string? hnCode { get; set; }
        public string? diagnosis { get; set; }
        public int? erId { get; set; }
        public int? assessRespiratoryId { get; set; }
        public string? assessCommentRespiratory { get; set; }
        public int? assessHemorrhageId { get; set; }
        public string? assessCommentHemorrhage { get; set; }
        public int? assessFluidId { get; set; }
        public string? assessCommentFluid { get; set; }
        public int? assessBoneId { get; set; }
        public string? assessCommentBone { get; set; }
        public int? assessorPosition { get; set; }
        public int? assessorId { get; set; }
        public string? assessorName { get; set; }
    }
    public class GreenOperationPreviewSection6
    {
        public int? isAdmit { get; set; }
        public int? finalResultId { get; set; }
        public string? stateDetail { get; set; }
        public int? stateId { get; set; }
        public int? stateIdcId { get; set; }
    }

    public class PinkOperationResultPreviewPage
    {
        public PinkOperationPreviewSection1? sec1 { get; set; }
        public PinkOperationPreviewSection2? sec2 { get; set; }
        public PinkOperationPreviewSection3? sec3 { get; set; }
        public PinkOperationPreviewSection4? sec4 { get; set; }
        public PinkOperationPreviewSection5? sec5 { get; set; }
        public PinkOperationPreviewSection6? sec6 { get; set; }
        public string? updated { get; set; }
        public string? updateBy { get; set; }
    }
    public class PinkOperationPreviewSection1
    {
        public int? eventId { get; set; }
        public int? eventOperationId { get; set; }
        public string? victimCode { get; set; }
        public string? eventTime { get; set; }
        public string? eventCode { get; set; }
        public string? operationCode { get; set; }
        public int? eventProvinceId { get; set; }
        public int? provinceId { get; set; }
        public int? departmentId { get; set; }
        public string? departmentName { get; set; }
        public int? vehicleId { get; set; }
        public int? staff4DocId { get; set; }
        public string? staff4DocName { get; set; }
        public int? staff5NurseId { get; set; }
        public string? staff5NurseName { get; set; }
        public int? staff1Id { get; set; }
        public string? staff1Name { get; set; }
        public int? staff2Id { get; set; }
        public string? staff2Name { get; set; }
        public int? staff3Id { get; set; }
        public string? staff3Name { get; set; }
        public int? stateIsFound { get; set; }
        public string? addrDetail { get; set; }
        public string? provinceName { get; set; }
        public int? districtId { get; set; }
        public string? districtName { get; set; }
        public int? subDistrictId { get; set; }
        public string? subDistrictName { get; set; }
        public string? eventRemark { get; set; }
    }
    public class PinkOperationPreviewSection2
    {
        public int? vehicleTypeId { get; set; }
        public int? departmentId { get; set; }
        public int? vehicleId { get; set; }
        public string? vehicleName { get; set; }
        public int? vehicleLevelId { get; set; }
        public string? vehicleLevelName { get; set; }
        public string? startPoint { get; set; }
        public string? stopPoint { get; set; }
        public string? time1Call { get; set; }
        public string? lat1Call { get; set; }
        public string? lon1Call { get; set; }
        public string? time2Command { get; set; }
        public string? lat2Command { get; set; }
        public string? lon2Command { get; set; }
        public string? time3OutBase { get; set; }
        public string? lat3OutBase { get; set; }
        public string? lon3OutBase { get; set; }
        public string? time4AtPoint { get; set; }
        public string? lat4AtPoint { get; set; }
        public string? lon4AtPoint { get; set; }
        public string? time5OutPoint { get; set; }
        public string? lat5OutPoint { get; set; }
        public string? lon5OutPoint { get; set; }
        public string? time6AtHos { get; set; }
        public string? lat6AtHos { get; set; }
        public string? lon6AtHos { get; set; }
        public string? time7AtBase { get; set; }
        public string? lat7AtBase { get; set; }
        public string? lon7AtBase { get; set; }
        public int? km1AtBase { get; set; }
        public int? km2AtPoint { get; set; }
        public int? km3AtHos { get; set; }
        public int? km4AtBase { get; set; }
    }
    public class PinkOperationPreviewSection3
    {
        public int? victimId { get; set; }
        public int? victimeTitleId { get; set; }
        public string? victimeTitleName { get; set; }
        public string? victimFirstname { get; set; }
        public string? victimLastname { get; set; }
        public int? victimAge { get; set; }
        public int? genderId { get; set; }
        public object? peopleTypeId { get; set; }
        public string? peopleIdCard { get; set; }
        public object? peopleCountryId { get; set; }
        public object? peopleCountryName { get; set; }
        public object? passport { get; set; }
        public object? treatmentRightId { get; set; }
        public int? isItravelId { get; set; }
        public object? itravelCountryId { get; set; }
        public object? itravelCountryName { get; set; }
        public object? icarCountryId { get; set; }
        public object? icarLabelGroup { get; set; }
        public object? icarLabelNumber { get; set; }
        public object? icarLabelProvinceId { get; set; }
        public object? icarLabelProvinceName { get; set; }
        public int? icarIsOwner { get; set; }
        public object? icarTypeId { get; set; }
        public object? icarTypeOwnerName { get; set; }
        public object? victimTypeId { get; set; }
        public string? a1Time { get; set; }
        public object? a1VsignT { get; set; }
        public object? a1VsignBp { get; set; }
        public object? a1VsignPr { get; set; }
        public object? a1VsignRr { get; set; }
        public object? a1NsignE { get; set; }
        public object? a1NsignV { get; set; }
        public object? a1NsignM { get; set; }
        public object? a1Dtx { get; set; }
        public object? sensationOk { get; set; }
        public object? sensationDull { get; set; }
        public object? sensationFaintOk { get; set; }
        public object? sensationFaintNo { get; set; }
        public object? sensationNoisy { get; set; }
        public object? breathNormal { get; set; }
        public object? breathFast { get; set; }
        public object? breathSlow { get; set; }
        public object? breathRough { get; set; }
        public object? breathNo { get; set; }
        public object? woundNo { get; set; }
        public object? woundAbrasion { get; set; }
        public object? woundCut { get; set; }
        public object? woundContusion { get; set; }
        public object? woundBurn { get; set; }
        public object? woundStab { get; set; }
        public object? woundGsw { get; set; }
        public object? woundAmputate { get; set; }
        public object? woundExplode { get; set; }
        public object? boneNo { get; set; }
        public object? boneYes { get; set; }
        public object? organHead { get; set; }
        public object? organFace { get; set; }
        public object? organSpine { get; set; }
        public object? organChest { get; set; }
        public object? organAbdomen { get; set; }
        public object? organPelvis { get; set; }
        public object? organExtremities { get; set; }
        public object? organExtSurface { get; set; }
        public object? organMultiinjuryBack { get; set; }
        public object? svBreathNo { get; set; }
        public object? svBreathClear { get; set; }
        public object? svBreathOral { get; set; }
        public object? svBreathO2 { get; set; }
        public object? svBreathAmbubag { get; set; }
        public object? svBreathPockedMask { get; set; }
        public object? svWoundNo { get; set; }
        public object? svWoundPressure { get; set; }
        public object? svWoundDressing { get; set; }
        public object? svBoneNo { get; set; }
        public object? svBoneSling { get; set; }
        public object? svBoneSpinal { get; set; }
        public object? svBoneKed { get; set; }
        public object? svCprNo { get; set; }
        public object? svCprYes { get; set; }
        public object? primaryResultId { get; set; }
    }
    public class PinkOperationPreviewSection4
    {
        public int? transferHospitalProvinceId { get; set; }
        public int? transferHospitalId { get; set; }
        public string? transferHospitalName { get; set; }
        public int? transferHospitalTypeId { get; set; }
        public int? transferReason1 { get; set; }
        public int? transferReason2 { get; set; }
        public int? transferReason3 { get; set; }
        public int? transferReason4 { get; set; }
        public int? transferReason5 { get; set; }
        public object? reportBy { get; set; }
        public object? reportByName { get; set; }
    }
    public class PinkOperationPreviewSection5
    {
        public string? hnCode { get; set; }
        public object? diagnosis { get; set; }
        public object? erId { get; set; }
        public object? assessRespiratoryId { get; set; }
        public object? assessCommentRespiratory { get; set; }
        public object? assessHemorrhageId { get; set; }
        public object? assessCommentHemorrhage { get; set; }
        public object? assessFluidId { get; set; }
        public object? assessCommentFluid { get; set; }
        public object? assessBoneId { get; set; }
        public object? assessCommentBone { get; set; }
        public object? assessorPosition { get; set; }
        public object? assessorId { get; set; }
        public object? assessorName { get; set; }
    }
    public class PinkOperationPreviewSection6
    {
        public object? isAdmit { get; set; }
        public object? finalResultId { get; set; }
        public int? stateId { get; set; }
        public int? stateIdcId { get; set; }
        public int? stateRcMainId { get; set; }
        public int? stateRcId { get; set; }
    }
}
