﻿namespace NiemsEBudget.Models
{
    public class GetProfileResult
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public Profile? result { get; set; }
    }
    public class Profile
    {
        public int? id { get; set; }
        public string? title { get; set; }
        public string? firstName { get; set; }
        public string? lastName { get; set; }
        public string? fulltName { get; set; }
        public int? department_ID { get; set; }
        public int? department_Province_ID { get; set; }
        public string? department_Str { get; set; }
        public string? department_Province_Str { get; set; }
        public List<Roles>? roles { get; set; }

    }
    public class Roles
    {
        public int? id { get; set; }
        public string? role { get; set; }
    }
}
