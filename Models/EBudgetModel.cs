﻿using System.ComponentModel.DataAnnotations;

namespace NiemsEBudget.Models
{
    public class EBResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
    }

    public class EBDefaultResponse : EBResponse
    {
        public string? result { get; set; }
    }

    public class EbudgetValuesRes : EBResponse
    {
        public List<EbudgetValues>? result { get; set; }  
    }
    public class EbudgetValues
    {
        public int? id { get; set; }
        public string? name { get; set; }
    }

    public class EBudgetModel
    {
        public string? token { get; set; }
        public int? s_Payment_Year { get; set; }
        public int? s_Payment_Schedule_ID { get; set; }
        public int? s_Province_ID { get; set; }
        public int? s_Department_ID { get; set; }
        public string? s_Operation_Code { get; set; }
        public int? s_Hospital_ID { get; set; }
        public int? s_Level_ID { get; set; }
        public string? s_T2_Op_Command_Str_start { get; set; }
        public string? s_T2_Op_Command_Str_end { get; set; }
        public int? s_Payment_Status_ID { get; set; }
        public string? o_By_Condition { get; set; }
        public int? s_Is_HaveAccount {  get; set; }
        public int? skip { get; set; }
        public int? take { get; set; }
    }
    public class EBudgetResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public EBudgetSearchResult? result { get; set; }
    }
    public class EBudgetSearchResult
    {
        public int? count { get; set; }
        public int? draw { get; set; }
        public int? recordsTotal { get; set; }
        public int? recordsFiltered { get; set; }
        public List<EBudgetSearchData>? data { get; set; }
    }
    public class EBudgetSearchData
    {
        public int? search_Result_Index { get; set; }
        public int? payment_ID { get; set; }
        public int? payment_Status_ID { get; set; }
        public string? payment_Status_Str { get; set; }
        public int? operation_ID { get; set; }
        public string? operation_Code { get; set; }
        public string? t2_Op_Command { get; set; }
        public string? t2_Op_Command_Str { get; set; }
        public int? department_ID { get; set; }
        public string? department_Str { get; set; }
        public int? level_ID { get; set; }
        public string? level_Str { get; set; }
        public int? vehicle_ID { get; set; }
        public string? vehicle_Str { get; set; }
        public int? service_ID { get; set; }
        public string? service_Str { get; set; }
        public int? recorder_Person_ID { get; set; }
        public string? recorder_Fullname { get; set; }
        public int? approver_Person_ID { get; set; }
        public string? approver_Fullname { get; set; }
        public string? victim_Code { get; set; }
        public string? victim_Fullname { get; set; }
        public string? victim_Transfer_Hospital { get; set; }
        public string? victim_HN { get; set; }
        public string? victim_ER_Str { get; set; }
        public string? victim_Final_Result_Str { get; set; }
        public int? payment_Amount { get; set; }
        public int? operation_Complete_Percentage { get; set; }
        public int? victim_Complete_Percentage { get; set; }
        public int? is_Paid { get; set; }
    }

    public class EbudgetSearchDuplicateOperation
    {
        public int? search_Result_Index { get; set; }
        public int? payment_Status_ID { get; set; }
        public string? payment_Status_Str { get; set; }
        public int? operation_ID { get; set; }
        public string? operation_Code { get; set; }
        public string? t2_Op_Command { get; set; }
        public string? t2_Op_Command_Str { get; set; }
        public int? department_ID { get; set; }
        public string? department_Str { get; set; }
        public int? vehicle_ID { get; set; }
        public string? vehicle_Str { get; set; }
        public int? level_ID { get; set; }
        public string? level_Str { get; set; }
        public int? service_ID { get; set; }
        public string? service_Str { get; set; }
        public int? recorder_Person_ID { get; set; }
        public string? recorder_Fullname { get; set; }
        public int? approver_Person_ID { get; set; }
        public string? approver_Fullname { get; set; }
        public string? victim_Code { get; set; }
        public string? victim_Fullname { get; set; }
        public string? victim_Transfer_Hospital { get; set; }
        public string? victim_HN { get; set; }
        public string? victim_ER_Str { get; set; }
        public string? victim_Final_Result_Str { get; set; }
        public int? schedule_Propose_ID { get; set; }
        public string? schedule_Propose_Str { get; set; }
        public string? schedule_Propose_ShortStr { get; set; }
        public int? schedule_Paid_ID { get; set; }
        public string? schedule_Paid_Str { get; set; }
        public string? schedule_Paid_ShortStr { get; set; }
        public int? payment_ID { get; set; }
        public int? payment_Amount { get; set; }
        public double? distance_Amount { get; set; }
        public int? operation_Complete_Percentage { get; set; }
        public int? victim_Complete_Percentage { get; set; }
        public int? is_Paid { get; set; }
        public int? paid_Payment_ID { get; set; }
        public double? paid_Distance_Amount { get; set; }
    }
    public class EbudgetSearchDuplicateOperationBody : EBudgetSearchData
    {
        public List<EbudgetSearchDuplicateOperation>? duplicateOperations { get; set; }
    }

    public class EbudgetSearchDuplicateOperationData
    {
        public int count { get; set; }
        public List<EbudgetSearchDuplicateOperationBody>? data { get; set; }
    }
    public class EbudgetSearchDuplicateOperationRes : EBResponse
    {
        public EbudgetSearchDuplicateOperationData? result { get; set; }
    }

    public class UpdatePaymentStatusData
    {
        public string? token { get; set; }
        //public List<int>? payment_IDs { get; set; }
        public List<int>? operation_IDs { get; set; }
        public int? to_Status_ID { get; set; }
    }
    public class UpdatePaymentStatusResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public string? result { get; set; }

    }

    public class PublishDepartmnetPaidReq
    {
        [Required(ErrorMessage ="Token is Invalid.")]
        public string? token { get; set; }
        public int schedule_ID { get; set; }
        public int doc_start_no {  get; set; }
        public string? doc_date_str {  get; set; }
        public string? doc_title {  get; set; }
        public string? finish_date_str { get; set; }
        public string? sign_pic_name {  get; set; }
        public string? sign_pic_ext {  get; set; }
        public string? sign_pic_str64 { get; set; }
        public string? sign_name {  get; set; }
        public string? sign_position {  get; set; }
        public string? responsible_name { get; set; }
    }

    public class PublishDepartmnetPaidRes : EBResponse
    {
        public string? result { get; set; } = null;
    }

    public class SetDepartmentPaymentInfoReq
    {
        public string? token { get; set; }
        public int? department_ID { get; set; }
        public string? mail_Position { get; set; }
        public string? pay_Department_Name { get; set; }
        public int? distance_KM { get; set; }
        public int? distance_Payrate { get; set; }
        public string? sign_FullName { get; set; }
        public string? sign_TelNo { get; set; }
        public string? contact_FullName { get; set; }
        public string? contact_TelNo { get; set; }
        public string? fax { get; set; }
        public string? email { get; set; }
        public int? bank_ID { get; set; }
        public string? bank_Branch { get; set; }
        public string? account_Name { get; set; }
        public int? account_Type_ID { get; set; }
        public string? account_No { get; set; }
        public string? taxPayID { get; set; }
        public int? is_Province_Direct_Pay { get; set; }
        public int? is_Department_Direct_Pay { get; set; }
        public int? is_With_Holding_Tax { get; set; }
        public int? is_Coorperate_Transfer { get; set; }
    }

    public class DepartmentPaymentInfo
    {
        public int? account_ID { get; set; }
        public int? department_ID { get; set; }
        public string? department_Code { get; set; }
        public string? department_Name { get; set; }
        public string? mail_Position { get; set; }
        public string? pay_Department_Name { get; set; }
        public int? distance_KM { get; set; }
        public int? distance_Payrate { get; set; }
        public string? sign_FullName { get; set; }
        public string? sign_TelNo { get; set; }
        public string? contact_FullName { get; set; }
        public string? contact_TelNo { get; set; }
        public string? fax { get; set; }
        public string? email { get; set; }
        public int? bank_ID { get; set; }
        public string? bank_Name { get; set; }
        public string? bank_Branch { get; set; }
        public string? account_Name { get; set; }
        public int? account_Type_ID { get; set; }
        public string? account_Type_Name { get; set; }
        public string? account_No { get; set; }
        public string? taxPayID { get; set; }
        public int? is_Province_Direct_Pay { get; set; }
        public int? is_Department_Direct_Pay { get; set; }
        public int? is_With_Holding_Tax { get; set; }
        public int? is_Coorperate_Transfer { get; set; }
    }

    public class GetDepartmentPaymentInfoRes : EBResponse
    {
        public DepartmentPaymentInfo? result { get; set; }
    }
    public class SetDepartmentPaymentInfoRes : EBResponse
    {
        public string? result { get; set; } = null;
    }

    public class SearchPaymentDocInfoRes : EBResponse
    {
        public SearchPaymentDocInfoResult? result { get; set; }
    }

    public class SearchPaymentDocInfoResult
    {
        public int? count { get; set; }
        public int? draw { get; set; }
        public int? recordsTotal { get; set; }
        public int? recordsFiltered { get; set; }
        public List<SearchPaymentDocInfo>? data { get; set; }
    }
    public class SearchPaymentDocInfo
    {
        public int? search_Result_Index { get; set; }
        public int? docInfo_ID { get; set; }
        public string? name_TH { get; set; }
        public string? account_Name { set; get; }
        public string? bank_Name { set; get; }
        public int? department_Count { get; set; }
    }

    public class GetPaymentDocInfoRes : EBResponse
    {
        public GetPaymentDocInfo? result { get; set; }
    }

    public class GetPaymentDocInfo
    {
        public int? docInfo_ID { get; set; }
        public string? name_TH { get; set; }
        public string? name_EN { get; set; }
        public string? doc_To { get; set; }
        public string? account_No { get; set; }
        public string? account_Name { get; set; }
        public int? bank_ID { get; set; }
        public string? bank_Name { get; set; }
        public int? is_Province_Direct_Pay { get; set; }
        public int? is_Department_Direct_Pay { get; set; }
        public int? is_With_Holding_Tax { get; set; }
        public string? taxPay_To { get; set; }
        public string? taxPay_PID { get; set; }
        public string? taxPay_Address { get; set; }
        public int? department_Count { get; set; }
    }

    public class AddPaymentDocInfoReq
    {
        public string? token { get; set; }
        public string? name_TH { get; set; }
        public string? name_EN { get; set; }
        public string? doc_To { get; set; }
        public string? account_No { get; set; }
        public string? account_Name { get; set; }
        public int? bank_ID { get; set; }
        public int? is_Province_Direct_Pay { get; set; }
        public int? is_Department_Direct_Pay { get; set; }
        public int? is_With_Holding_Tax { get; set; }
        public string? taxPay_To { get; set; }
        public string? taxPay_PID { get; set; }
        public string? taxPay_Address { get; set; }
    }

    public class UpdatePaymentDocInfoReq
    {
        public string? token { get; set; }
        public int? docInfo_ID { get; set; }
        public string? name_TH { get; set; }
        public string? name_EN { get; set; }
        public string? doc_To { get; set; }
        public string? account_No { get; set; }
        public string? account_Name { get; set; }
        public int? bank_ID { get; set; }
        public int? is_Province_Direct_Pay { get; set; }
        public int? is_Department_Direct_Pay { get; set; }
        public int? is_With_Holding_Tax { get; set; }
        public string? taxPay_To { get; set; }
        public string? taxPay_PID { get; set; }
        public string? taxPay_Address { get; set; }
    }

    //public class DocInfoDepartmentReq
    //{
    //    public string? token { get; set; }
    //    public int? docInfo_ID { get; set; }
    //    public string? s_Keyword { get; set; }
    //    public string? o_By_Condition { get; set; }
    //    public int? skip { get; set; }
    //    public int? take { get; set; }
    //}

    public class DocInfoDepartmentRes : EBResponse
    {
        public DocInfoDepartmentResult? result {  get; set; }
    }

    public class DocInfoDepartmentResult
    {
        public int? count { get; set; }
        public int? draw { get; set; }
        public int? recordsTotal { get; set; }
        public int? recordsFiltered { get; set; }
        public List<DocInfoDepartment>? data { get; set;}
    }

    public class DocInfoDepartment
    {
        public int? search_Result_Index { get; set; }
        public int? record_Id { get; set; }
        public int? department_ID { get; set; }
        public string? department_Name { get; set; }
        public string? department_Code { get; set; }
        public string? department_HCODE { get; set; }
        public int? is_Department_Enable { get; set; }
    }

    public class AddDepartmentsToPaymentDocInfoReq
    {
        public string? token { get; set; }
        public int? docInfo_ID {  get; set; }
        public List<int>? department_IDs { get; set; }
    }
}