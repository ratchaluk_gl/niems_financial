﻿namespace NiemsEBudget.Models
{
    public class HomeModelcs
    {
    }
    //public class DataTableApprovedOperationDetailResult
    // {
    //     public bool? callAPIStatus { get; set; }
    //     public string? callAPIStatusMessage { get; set; }
    //     public int? count { get; set; }
    //     public int? draw { get; set; }
    //     public int? recordsTotal { get; set; }
    //     public int? recordsFiltered { get; set; }
    //     public List<DataTableApprovedOperationDetail>? data { get; set; }
    // }
    // public class DataTableApprovedOperationDetail
    // {
    //     public int? eventOperationId { get; set; }
    //     public int? eventId { get; set; }
    //     public int? eventStatusId { get; set; }
    //     public string? eventStatusName { get; set; }
    //     public string? eventOperationCode { get; set; }
    //     public string? commandDate { get; set; }
    //     public string? vehicleDetail { get; set; }
    //     public string? vehicleLevel { get; set; }
    //     public string? departmentCode { get; set; }
    //     public string? departmentName { get; set; }
    //     public string? service { get; set; }
    //     public string? createdBy { get; set; }
    //     public string? approvedBy { get; set; }
    //     public int? completedInfoPercentage { get; set; }
    //     public int? isUpdated { get; set; }
    //     public int? eventVictimId { get; set; }
    //     public List<MainVictim>? victims { get; set; }

    // }
    // public class MainVictim
    // {
    //     public int? id { get; set; }
    //     public int? eventId { get; set; }
    //     public string? code { get; set; }
    //     public string? name { get; set; }
    //     public string? transferHospitalName { get; set; }
    //     public string? hn { get; set; }
    //     public int? erStatus { get; set; }
    //     public int? finalResult { get; set; }
    //     public string? passportNumber { get; set; }
    //     public int? provinceId { get; set; }
    //     public string? provinceName { get; set; }
    //     public int? serviceId { get; set; }
    //     public int? vehicleLevelId { get; set; }
    //     public int? completedInfoPercentage { get; set; }
    //     public int? isMain { get; set; }
    //     public double? paymentAmount { get; set; }
    //     public int? stateCount { get; set; }

    // }

    // public class GetOperationByProposePayment
    // {
    //     public string? token { get; set; }
    //     public int? keywordType { get; set; }
    //     public string? keyword { get; set; }
    //     public int? s_Province_id { get; set; }
    //     public string? s_Date_Start { get; set; }
    //     public string? s_Date_To { get; set; }
    //     public int? s_Statement_Status_Id { get; set; }
    //     public int? s_Level_Id { get; set; }
    //     public int? s_Service_Id { get; set; }
    //     public int? s_ER_Id { get; set; }
    //     public int? s_Duplicate_Id { get; set; }
    //     public int? s_Is_Approved { get; set; }
    //     public string? o_By_Condition { get; set; }
    //     public int? skip { get; set; }
    //     public int? take { get; set; }

    // }

    public class ScheduleListResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public List<ScheduleList>? result { get; set; }
    }
    public class ScheduleList
    {
        public int? schedule_ID { get; set; }
        public int? schedule_Year { get; set; }
        public int? schedule_Month { get; set; }
        public int? schedule_Round { get; set; }
        public string? schedule_Detail { get; set; }
        public string? schedule_Name { get; set; }

    }
    public class AddressObjectResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public List<AddressDetail>? result { get; set; }
    }
    public class AddressDetail
    {
        public int? id { get; set; }
        public string? code { get; set; }
        public string? name { get; set; }
    }
    public class HospitalInProvinceResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public List<HospitalDetail>? result { get; set; }
    }

    public class HospitalDetail
    {
        public int? id { get; set; }
        public string? name { get; set; }
        public string? code { get; set; }
        public string? hcode { get; set; }
    }
    public class DepartmentResult
    {
        public bool callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public List<Department>? result { get; set; }
    }
    public class Department
    {
        public int id { get; set; }
        public string? code { get; set; }
        public string? hcode { get; set; }
        public string? name { get; set; }
    }

    public class PaidReportResponse
    {
        public bool? callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public FileResult? result { get; set;}
    }
    public class FileResult
    {
        public string? fileName { get; set; }
        public string? fileType { get; set; }
        public string? fileContent { get; set; }
        public byte[]? fileContentBytes { get; set; }
    }

    
}

