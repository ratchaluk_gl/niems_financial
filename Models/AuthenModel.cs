﻿using System.ComponentModel.DataAnnotations;

namespace NiemsEBudget.Models
{
    public class AuthenModel
    {
       

    }
    public class LoginResult
    {
        public Boolean callAPIStatus { get; set; }
        public string? callAPIStatusMessage { get; set; }
        public AuthAPIResult? Result { get; set; }
    }
    public class AuthAPIResult
    {
        public string? jwtToken { get; set; }
        public string? webToken { get; set; }
        public string? webToken_Exprire_Date { get; set; }
    }
    public class LoginData
    {
        [Required(ErrorMessage = "กรุณากรอกชื่อผู้ใช้")]
        public string username { get; set; }
        [Required(ErrorMessage = "กรุณากรอกรหัสผ่าน")]
        public string password { get; set; }
        public string? application_code { get; set; }
    }
    public class TokenResponse
    {
        public bool? result { get; set; }
        public string? message { get; set; }
        public string? tok { get; set; }
        public string? jwt { get; set; }
    }

}
